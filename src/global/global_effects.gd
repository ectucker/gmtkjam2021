extends CanvasLayer


var transitioning = false
var target

func transition_to(scene):
	if not transitioning:
		transitioning = true
		target = scene
		$AnimationPlayer.play("Transition")

func actual_change():
	get_tree().change_scene(target)
	transitioning = false

func _input(event):
	if event.is_action_pressed("toggle_fullscreen"):
		OS.window_fullscreen = !OS.window_fullscreen
