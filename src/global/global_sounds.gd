extends Node


func play(sound):
	get_node(sound).play()

func play_if_not(sound):
	get_node(sound).play_if_not()
