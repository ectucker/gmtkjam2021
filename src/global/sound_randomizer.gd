extends Node

export var min_pitch_scale: float = 1.0
export var max_pitch_scale: float = 1.0

func play():
	var chosen_index = randi() % get_child_count()
	var chosen_sound = get_child(chosen_index)
	chosen_sound.pitch_scale = rand_range(min_pitch_scale, max_pitch_scale)
	chosen_sound.play()
	
	#var instanced_player = AudioStreamPlayer.new()
	#get_parent().add_child(instanced_player)
	#instanced_player.stream = chosen_sound.stream
	#instanced_player.volume_db = chosen_sound.volume_db
	#instanced_player.pitch_scale = chosen_sound.pitch_scale
	#instanced_player.play()
	#instanced_player.connect("finished", instanced_player, "queue_free")

func play_if_not():
	if not is_playing():
		play()

func is_playing():
	for child in get_children():
		if child.playing:
			return true
	return false
