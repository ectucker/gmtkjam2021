extends Node


signal round_changed


var tile_list = [
	preload("res://src/blocks/center.tscn"),
	preload("res://src/blocks/wood.tscn"),
	preload("res://src/blocks/joint.tscn"),
	preload("res://src/blocks/spikes.tscn"),
	preload("res://src/blocks/thruster.tscn"),
	preload("res://src/blocks/turret.tscn")
]

var tile_cares_rotation = [
	false,
	false,
	false,
	true,
	true,
	true
]

var in_editor = true

var tile_layout: Array2D = null
var rotation_layout: Array2D = null

var tile_layout_top: Array2D = null
var rotation_layout_top: Array2D = null

var cur_wave = 1
var max_wave = 0

var living_enemies = 0
