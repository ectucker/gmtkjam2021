extends TextureButton

export var weapon: NodePath
onready var weapon_layer = get_node(weapon)

var time_since_press = 0

func _ready():
	connect("pressed", self, "_pressed")

func _process(delta):
	time_since_press += delta

func _pressed():
	if time_since_press > 0.05:
		weapon_layer.rotate_right()
		GlobalSounds.play("UIButton")
	time_since_press = 0
