extends TextureButton

export var tile_preview_path: NodePath
onready var tile_preview = get_node(tile_preview_path)

var time_since_press = 0

func _ready():
	connect("pressed", self, "_pressed")

func _process(delta):
	time_since_press += delta

func _pressed():
	if time_since_press > 0.05:
		tile_preview.erase_mode = not tile_preview.erase_mode
		GlobalSounds.play("UIButton")
	time_since_press = 0
