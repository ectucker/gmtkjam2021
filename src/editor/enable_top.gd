extends TextureButton


export var top_layer_path: NodePath
onready var top_layer = get_node(top_layer_path)

export var bottom_layer_path: NodePath
onready var bottom_layer = get_node(bottom_layer_path)

export var tile_preview_path: NodePath
onready var tile_preview = get_node(tile_preview_path)


func _ready():
	connect("pressed", self, "_pressed")

func _pressed():
	top_layer.active = true
	bottom_layer.active = false
	
	top_layer.modulate = Color.white
	bottom_layer.modulate = Color.navyblue
	
	tile_preview.weapon = top_layer
	
	GlobalSounds.play("UIButton")
