extends TextureButton


export var weapon: NodePath

onready var weapon_layer = get_node(weapon)

func _ready():
	connect("pressed", self, "_pressed")

func _pressed():
	weapon_layer.start_round()
	GlobalSounds.play("UIButton")
