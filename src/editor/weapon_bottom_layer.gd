extends Node2D


export var tile_preview_path: NodePath
onready var tile_preview = get_node(tile_preview_path)

export var top_layer_path: NodePath
onready var top_layer = get_node(top_layer_path)

var selected_tile = 1 setget set_selected_tile

var selected_rotation = 0

var tile_layout: Array2D = Array2D.new()
var rotation_layout: Array2D = Array2D.new()

var joint_position = null

var active = true

func _ready():
	if GlobalData.tile_layout != null:
		tile_layout = GlobalData.tile_layout
	else:
		tile_layout.resize(11, 11)
		tile_layout.set_cell(5, 5, 0)
	if GlobalData.rotation_layout != null:
		rotation_layout = GlobalData.rotation_layout
	else:
		rotation_layout.resize(11, 11)
		rotation_layout.set_cell(5, 5, 0)
	joint_position = null
	for x in range(0, 11):
		for y in range(0, 11):
			if tile_layout.get_cell(x, y) == 2:
				joint_position = Vector2(x, y)
	_refresh_children()
	GlobalData.in_editor = true

func _process(delta):
	global_position.x = -32 * 5
	global_position.y = -32 * 5

func get_mouse_tile_pos():
	var tile_position = (get_global_mouse_position() - global_position) / 32
	tile_position.x = round(tile_position.x)
	tile_position.y = round(tile_position.y)
	return tile_position

func _input(event):
	if active:
		if event.is_action_pressed("editor_place") || event is InputEventMouseMotion:
			if Input.is_action_pressed("editor_place"):
				var tile_position = get_mouse_tile_pos()
				if not tile_preview.erase_mode:
					if is_valid_placement(tile_position, selected_rotation):
						tile_layout.set_cellv(tile_position, selected_tile)
						rotation_layout.set_cellv(tile_position, selected_rotation)
						_refresh_children()
						if selected_tile == 2:
							top_layer.tile_layout.set_cellv(tile_position, selected_tile)
							top_layer.rotation_layout.set_cellv(tile_position, selected_rotation)
							top_layer._refresh_children()
							joint_position = tile_position
							top_layer.joint_position = tile_position
						GlobalSounds.play("BlockPlace")
				else:
					if has_tile(tile_position):
						tile_layout.set_cellv(tile_position, null)
						_remove_unconnected()
						_refresh_children()
						GlobalSounds.play("BlockRemove")
	

func start_round():
	GlobalData.tile_layout = tile_layout
	GlobalData.rotation_layout = rotation_layout
	top_layer.start_round()
	GlobalEffects.transition_to("res://src/arena/arena.tscn")
	GlobalSounds.play_if_not("AssemblyMontage")
	GlobalData.in_editor = false

func has_tile(cellv):
	if cellv.x == 5 and cellv.y == 5:
		return false
	if not tile_layout.has_cellv(cellv):
		return false
	return tile_layout.get_cellv(cellv) != null

func is_valid_placement(cellv, rotation):
	if not tile_layout.has_cellv(cellv):
		return false
	if tile_layout.get_cellv(cellv) != null:
		return false
	if not _has_adjacent_connector(cellv):
		return false
	if GlobalData.tile_cares_rotation[selected_tile]:
		var rotated_dir = Vector2.DOWN.rotated(deg2rad(selected_rotation))
		rotated_dir.x = round(rotated_dir.x)
		rotated_dir.y = round(rotated_dir.y)
		if !tile_layout.has_cellv(cellv + rotated_dir):
			return false
		if tile_layout.get_cellv(cellv + rotated_dir) == null:
			return false
		if tile_layout.get_cellv(cellv + rotated_dir) > 2:
			return false
	if selected_tile == 2 and joint_position != null:
		return false
	return true

func _has_adjacent_connector(cellv):
	if tile_layout.has_cellv(cellv + Vector2.UP):
		var up = tile_layout.get_cellv(cellv + Vector2.UP)
		if up != null and up < 3:
			return true
	if tile_layout.has_cellv(cellv + Vector2.DOWN):
		var down = tile_layout.get_cellv(cellv + Vector2.DOWN)
		if down != null and down < 3:
			return true
	if tile_layout.has_cellv(cellv + Vector2.LEFT):
		var left = tile_layout.get_cellv(cellv + Vector2.LEFT)
		if left != null and left < 3:
			return true
	if tile_layout.has_cellv(cellv + Vector2.RIGHT):
		var right = tile_layout.get_cellv(cellv + Vector2.RIGHT)
		if right != null and right < 3:
			return true

func _remove_unconnected():
	# Flood fill to find everything connected to start
	var flooded = Array2D.new()
	flooded.resize(11, 11)
	var queue = []
	queue.push_back(Vector2(5, 5))
	while not queue.empty():
		var cellv = queue.pop_front()
		flooded.set_cellv(cellv, 1)
		if tile_layout.get_cellv(cellv) < 3 :
			if tile_layout.has_cellv(cellv + Vector2.UP) and tile_layout.get_cellv(cellv + Vector2.UP) != null and flooded.get_cellv(cellv + Vector2.UP) != 1:
				queue.push_back(cellv + Vector2.UP)
			if tile_layout.has_cellv(cellv + Vector2.DOWN) and tile_layout.get_cellv(cellv + Vector2.DOWN) != null and flooded.get_cellv(cellv + Vector2.DOWN) != 1:
				queue.push_back(cellv + Vector2.DOWN)
			if tile_layout.has_cellv(cellv + Vector2.LEFT) and tile_layout.get_cellv(cellv + Vector2.LEFT) != null and flooded.get_cellv(cellv + Vector2.LEFT) != 1:
				queue.push_back(cellv + Vector2.LEFT)
			if tile_layout.has_cellv(cellv + Vector2.RIGHT) and tile_layout.get_cellv(cellv + Vector2.RIGHT) != null and flooded.get_cellv(cellv + Vector2.RIGHT) != 1:
				queue.push_back(cellv + Vector2.RIGHT)
	
	for x in range(0, 11):
		for y in range(0, 11):
			if flooded.get_cell(x, y) == null:
				tile_layout.set_cell(x, y, null)
				rotation_layout.set_cell(x, y, null)
	for x in range(0, 11):
		for y in range(0, 11):
			if tile_layout.get_cell(x, y) != null:
				var directional = GlobalData.tile_cares_rotation[tile_layout.get_cell(x, y)]
				if directional:
					var rotated_dir = Vector2.DOWN.rotated(deg2rad(rotation_layout.get_cell(x, y)))
					rotated_dir.x = round(rotated_dir.x)
					rotated_dir.y = round(rotated_dir.y)
					if tile_layout.get_cellv(Vector2(x, y) + rotated_dir) == null:
						tile_layout.set_cell(x, y, null)
	
	joint_position = null
	for x in range(0, 11):
		for y in range(0, 11):
			if tile_layout.get_cell(x, y) == 2:
				joint_position = Vector2(x, y)
	if joint_position == null:
		top_layer.joint_position = null
		top_layer._remove_unconnected()
		top_layer._refresh_children()

func _refresh_children():
	for child in get_children():
		remove_child(child)
		child.queue_free()
	
	for x in range(0, 11):
		for y in range(0, 11):
			var tile = tile_layout.get_cell(x, y)
			if tile != null:
				var placed = GlobalData.tile_list[tile].instance()
				add_child(placed)
				placed.position = Vector2(x, y) * 16
				placed.rotation_degrees = rotation_layout.get_cell(x, y)

func set_selected_tile(var value):
	selected_tile = value
	tile_preview.set_tile(value)

func rotate_right():
	selected_rotation += 90
	selected_rotation %= 360
	tile_preview.rotation_degrees = selected_rotation
	#tile_preview.set_tile(value)

func rotate_left():
	selected_rotation += 360 - 90
	selected_rotation %= 360
	tile_preview.rotation_degrees = selected_rotation
