extends Sprite


export var weapon_path: NodePath
onready var weapon = get_node(weapon_path)

var erase_mode = false setget set_erase_mode
var eraser_tooltip = preload("res://assets/ui/eraser_tooltip.png")

var tile_list = [
	preload("res://assets/blocks/center.png"),
	preload("res://assets/blocks/wood.png"),
	preload("res://assets/blocks/joint.png"),
	preload("res://assets/blocks/spikes.png"),
	preload("res://assets/blocks/thruster.png"),
	preload("res://assets/blocks/turret.png")
]
var tile_texture = tile_list[0]


func _process(delta):
	var tile_position = weapon.get_mouse_tile_pos()
	global_position = tile_position * 32
	global_position.x += -32 * 5
	global_position.y += -32 * 5
	
	if not erase_mode:
		if weapon.is_valid_placement(tile_position, rotation_degrees):
			modulate = Color.white
		else:
			modulate = Color.red
	else:
		if weapon.has_tile(tile_position):
			modulate = Color.white
		else:
			modulate = Color.red

func set_tile(tile):
	erase_mode = false
	tile_texture = tile_list[tile]
	texture = tile_list[tile]

func set_erase_mode(value):
	erase_mode = value
	if erase_mode:
		texture = eraser_tooltip
	else:
		texture = tile_texture
