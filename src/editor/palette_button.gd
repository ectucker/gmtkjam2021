extends TextureButton

export var weapon: NodePath
export var index: int = 1

onready var weapon_layer = get_node(weapon)

func _ready():
	connect("pressed", self, "_pressed")

func _pressed():
	weapon_layer.selected_tile = index
	GlobalSounds.play("UIButton")
