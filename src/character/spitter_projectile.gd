extends Area2D


var direction = Vector2.ZERO

var time = 0


func _ready():
	connect("body_entered", self, "_body_entered")

func _body_entered(body):
	if not body.is_in_group("enemies"):
		if body.has_method("damage"):
			body.damage(1)
		queue_free()

func _physics_process(delta):
	global_position += direction * 256.0 * delta
	rotate(delta)
	time += delta
	if time > 2.0:
		queue_free()
