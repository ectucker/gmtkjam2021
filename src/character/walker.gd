extends RigidBody2D


var target

var wander_time = 0
var wander_target = Vector2.ZERO

var health = 1

var blood_scene = preload("res://src/character/blood_particle.tscn")


func _ready():
	add_to_group("enemies")
	GlobalData.living_enemies += 1
	target = get_tree().get_nodes_in_group("players")[0]
	GlobalData.connect("round_changed", self, "queue_free")

func _physics_process(delta):
	if health > 0:
		var impulse_dir = Vector2.RIGHT
		if target != null:
			impulse_dir = (target.global_position - global_position).normalized()
			apply_central_impulse(impulse_dir * 10.0)
		else:
			# Wander
			wander_time -= delta
			if wander_time <= 0:
				wander_time = rand_range(2.0, 3.0)
				wander_target = global_position + Vector2(rand_range(-96, 96), rand_range(-96, 96))
			impulse_dir = (wander_target - global_position).normalized()
			if linear_velocity.length() < 5.0 || linear_velocity.dot(impulse_dir) > 0.3:
				apply_central_impulse(impulse_dir * 2.0)
		
		$Sprite.scale.x = sign(impulse_dir.x)

func _integrate_forces(state):
	state.linear_velocity = state.linear_velocity * 0.8

func damage(amount):
	health -= amount
	if health == 0:
		GlobalSounds.play("ZombieHurt")
		$AnimationPlayer.play("Dead")
		collision_mask = 1
		$DamageArea.queue_free()
		GlobalData.living_enemies -= 1
		mass = 0.2
		var blood = blood_scene.instance()
		get_parent().add_child(blood)
		blood.global_position = global_position
