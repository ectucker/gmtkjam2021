extends RigidBody2D


var target

var wander_time = 0
var wander_target = Vector2.ZERO

var health = 1

var time = 0

var projectile_scene = preload("res://src/character/spitter_projectile.tscn")
var blood_scene = preload("res://src/character/blood_particle.tscn")


func _ready():
	add_to_group("enemies")
	GlobalData.living_enemies += 1
	target = get_tree().get_nodes_in_group("players")[0]
	GlobalData.connect("round_changed", self, "queue_free")

func _physics_process(delta):
	if health > 0:
		var impulse_dir = Vector2.RIGHT
		if target != null:
			if (target.global_position - global_position).length() > 96:
				impulse_dir = (target.global_position - global_position).normalized()
			elif (target.global_position - global_position).length() < 64:
				impulse_dir = -(target.global_position - global_position).normalized()
			else:
				impulse_dir = Vector2.ZERO
			apply_central_impulse(impulse_dir * 5.0)
		
		if impulse_dir.x != 0:
			$Sprite.scale.x = sign(impulse_dir.x)
		
		time += delta
		if time > 1.4:
			var projectile = projectile_scene.instance()
			projectile.global_position = $MouthPos.global_position
			projectile.direction = (target.global_position - global_position).normalized()
			get_parent().get_parent().add_child(projectile)
			time = 0
			GlobalSounds.play("SpitterAttack")

func _integrate_forces(state):
	state.linear_velocity = state.linear_velocity * 0.8

func damage(amount):
	health -= amount
	if health == 0:
		GlobalSounds.play("ZombieHurt")
		$AnimationPlayer.play("Dead")
		collision_mask = 1
		GlobalData.living_enemies -= 1
		mass = 0.1
		var blood = blood_scene.instance()
		get_parent().add_child(blood)
		blood.global_position = global_position
