extends Area2D


export var exclude = "none"

var to_damage = []


func _ready():
	connect("body_entered", self, "_body_entered")
	connect("body_exited", self, "_body_exited")

func _body_entered(body):
	if not body.is_in_group(exclude):
		if body.has_method("damage"):
			to_damage.append(body)

func _body_exited(body):
	if not body.is_in_group(exclude):
		if body.has_method("damage"):
			to_damage.erase(body)

func _physics_process(delta):
	for thing in to_damage:
		if thing.damage(1):
			GlobalSounds.play("WalkerAttack")
