extends RigidBody2D


var health = 5


func _ready():
	add_to_group("players")
	GlobalData.connect("round_changed", self, "round_heal")

func _integrate_forces(state):
	if health > 0:
		var x_dir = Input.get_action_strength("move_right") - Input.get_action_strength("move_left")
		var y_dir = Input.get_action_strength("move_down") - Input.get_action_strength("move_up")
		var dir_mod = abs((Vector2(x_dir, y_dir).angle() - linear_velocity.angle())) / PI
		apply_central_impulse(Vector2(x_dir, y_dir).normalized() * (450))
	state.linear_velocity = state.linear_velocity * 0.95

func damage(amount):
	if not $EffectsAnimation.is_playing():
		if health > 0:
			health -= amount
			if health <= 0:
				$AnimationPlayer.play("Dead")
				GlobalSounds.play("PlayerDeath")
			else:
				$EffectsAnimation.play("Hurt")
				GlobalSounds.play("PlayerHurt")
			return true
	return false

func round_heal():
	health = 5
