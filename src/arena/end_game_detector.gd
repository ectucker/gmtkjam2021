extends AnimationPlayer


export var player_path: NodePath
onready var player = get_node(player_path)

var shown = false


func _process(delta):
	if player.health <= 0 and not shown:
		play("Show")
		shown = true

func return_to_builder():
	GlobalEffects.transition_to("res://src/editor/weapon_editor.tscn")
