extends Node2D


export var round_starter_path: NodePath
onready var round_starter = get_node(round_starter_path)

export var player_path: NodePath
onready var player = get_node(player_path)

var walker = preload("res://src/character/walker.tscn")
var spitter = preload("res://src/character/spitter.tscn")


func _ready():
	spawn_wave(1)
	GlobalData.cur_wave = 1

func _process(delta):
	if GlobalData.living_enemies <= 0 and player.health > 0:
		GlobalData.max_wave = max(GlobalData.max_wave, GlobalData.cur_wave)
		GlobalData.cur_wave += 1
		spawn_wave(GlobalData.cur_wave)

func spawn_wave(num):
	GlobalData.emit_signal("round_changed")
	GlobalData.living_enemies = 0
	var num_walkers = 2 * pow(num,1.8)
	var num_spitters = 1 * pow(num,1.8)
	for i in range(0, num_walkers):
		var spawn = walker.instance()
		spawn.global_position = Vector2(rand_range(-500, 500), rand_range(-500, 500))
		add_child(spawn)
	for i in range(0, num_spitters):
		var spawn = spitter.instance()
		spawn.global_position = Vector2(rand_range(-500, 500), rand_range(-500, 500))
		add_child(spawn)
	round_starter.play("Show")
	if num != 1:
		GlobalSounds.play_if_not("WaveChange")
