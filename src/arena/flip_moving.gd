extends Sprite


func _process(delta):
	if get_parent().health > 0:
		var x_dir = Input.get_action_strength("ui_right") - Input.get_action_strength("ui_left")
		if x_dir != 0:
			scale.x = sign(x_dir)
