extends Line2D


export var target_1: NodePath
export var target_2: NodePath

onready var target_1_instance = get_node(target_1)
onready var target_2_instance = get_node(target_2)


func _process(delta):
	points[0] = target_1_instance.global_position
	points[1] = target_2_instance.global_position
