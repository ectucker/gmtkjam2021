extends HBoxContainer

export var player_path: NodePath
onready var player = get_node(player_path)

var heart_scene = preload("res://src/arena/heart.tscn")

func _process(delta):
	if player.health >= 0:
		while player.health < get_child_count():
			var remove = get_child(0)
			remove_child(remove)
			remove.queue_free()
		while player.health > get_child_count():
			var add = heart_scene.instance()
			add_child(add)
