extends RigidBody2D


func _ready():
	for x in range(0, 11):
		for y in range(0, 11):
			var tile = GlobalData.tile_layout.get_cell(x, y)
			if tile != null:
				var placed = GlobalData.tile_list[tile].instance()
				add_child(placed)
				placed.global_position = Vector2(x, y) * 16 - Vector2(5, 5) * 16
				placed.rotation_degrees = GlobalData.rotation_layout.get_cell(x, y)
