extends RigidBody2D


export var bottom: NodePath
export var joint_path: NodePath
onready var joint = get_node(joint_path)


func _ready():
	for x in range(0, 11):
		for y in range(0, 11):
			var tile = GlobalData.tile_layout_top.get_cell(x, y)
			if tile != null:
				var placed = GlobalData.tile_list[tile].instance()
				add_child(placed)
				placed.global_position = Vector2(x, y) * 16 - Vector2(5, 5) * 16
				placed.rotation_degrees = GlobalData.rotation_layout_top.get_cell(x, y)
				
				if tile == 2:
					joint.global_position = placed.global_position
					joint.node_a = get_path()
					joint.node_b = bottom
