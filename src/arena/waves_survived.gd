extends Label


func _process(delta):
	var survived = GlobalData.cur_wave - 1
	if survived != 1:
		text = "You survived %s waves." % (survived)
	else:
		text = "You survived 1 wave."
	if survived == GlobalData.max_wave:
		text += "\nYour record!"
	else:
		text += "\nNow back to the drawing board."
