extends Button

var time_since_press = 0


func _process(delta):
	time_since_press += delta

func _pressed():
	if time_since_press > 0.05:
		GlobalEffects.transition_to("res://src/editor/weapon_editor.tscn")
		GlobalSounds.play("UIButton")
	time_since_press = 0
