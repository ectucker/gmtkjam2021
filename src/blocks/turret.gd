extends Node2D


var projectile_scene = preload("res://src/blocks/turret_projectile.tscn")


func shoot():
	if not GlobalData.in_editor:
		var projectile = projectile_scene.instance()
		projectile.direction = Vector2.UP.rotated(global_rotation)
		projectile.global_position = $ProjectileSpawn.global_position
		get_parent().get_parent().add_child(projectile)
		GlobalSounds.play("TurretFire")
		if get_parent().has_method("apply_impulse"):
			get_parent().apply_impulse(position, Vector2.DOWN.rotated(rotation) * 50)
