extends Node2D


func _physics_process(delta):
	if get_parent().has_method("apply_impulse"):
		get_parent().apply_impulse(position, Vector2.DOWN.rotated(rotation) * 5)
